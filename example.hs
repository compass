{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}
import Math.Compass

import Diagrams.Prelude hiding (circle)
import qualified Diagrams.Prelude as D
import Diagrams.Backend.SVG.CmdLine (defaultMain)

import Data.Complex
import Data.Maybe (fromJust)

point (EPoint (x :+ y)) = D.circle 0.02 # translate (r2 (x, y))
curve (ELine (EPoint z) (EPoint w)) = fromVertices [ p2 (px, py), p2 (qx, qy) ]
  where
    px :+ py = z + 4 * (w - z)
    qx :+ qy = w + 4 * (z - w)
curve (ECircle (EPoint (x :+ y)) r) = D.circle r # translate (r2 (x, y))

example embedding = mconcat (map point [a0, b0, c0, d0, e0] ++ map curve [l0, m0, n0])
  where
    a0 = EPoint ((-0.5) :+ 0.25)
    b0 = EPoint (  0.5  :+ 0.5 )
    c0 = EPoint ((-0.5) :+ 0.5 )
    d0 = EPoint (  0.5  :+ 0.25)
    Just a = unembedPoint embedding a0
    Just b = unembedPoint embedding b0
    Just c = unembedPoint embedding c0
    Just d = unembedPoint embedding d0
    l = line a b
    m = line c d
    n = circle e a
    [e] = intersect l m
    l0 = embedConstruct embedding l
    m0 = embedConstruct embedding m
    n0 = embedConstruct embedding n
    e0 = embedPoint embedding e

main = defaultMain . hcat . map (pad 1.1 . centerXY . lc black . lw 0.002 . withEnvelope' s . clipBy' s . stroke) $
  [ example euclidean :: Path V2 Double
  , example poincareDisc `mappend` D.circle 1
  , example poincareHalfPlane `mappend` hrule 4
--  , example stereographic
  ]
  where
    s = square 2.2
    withEnvelope' x y = withEnvelope (x `asTypeOf` y) y
    clipBy' x y = withEnvelope (x `asTypeOf` y) y
