{-# LANGUAGE TypeFamilies, StandaloneDeriving, FlexibleInstances #-}
module Math.Compass.Hyperbolic where

import Data.Complex (Complex((:+)), magnitude, phase, polar, mkPolar, cis)
import Data.Maybe (mapMaybe)

import Math.Compass.Class
import Math.Compass.Euclidean

data H r = H

poincareDisc :: RealFloat r => Embed (H r) (E r)
poincareDisc = Embed
  { embedPoint = \(HPoint z w) -> EPoint (z / w)
  , unembedPoint = \(EPoint z) -> if magnitude z < 1 then Just (HPoint z 1) else Nothing
  , embedConstruct = \c -> case c of
      HLine p q
        | a2 < 1e-12 || abs d < 1e-12 -> line (embedPoint poincareDisc p) (embedPoint poincareDisc q)
        | otherwise -> circle (EPoint (ux :+ uy)) (embedPoint poincareDisc p)
        where
          ax :+ ay = fromPoint p
          bx :+ by = fromPoint q
          a2 = ax * ax + ay * ay
          b2 = bx * bx + by * by
          cx = ax / a2
          cy = ay / a2
          c2 = cx * cx + cy * cy;
          d = 2 * (ax * (by - cy) + bx * (cy - ay) + cx * (ay - by))
          ux = (a2*(by - cy) + b2*(cy - ay) + c2*(ay - by)) / d
          uy = (a2*(cx - bx) + b2*(ax - cx) + c2*(bx - ax)) / d
          dx = ux - bx
          dy = uy - by
          r = sqrt(dx * dx + dy * dy)
      HCircle centre radius -> circle centre' p'
        where
          v = (normalize . vector) centre
          p = transform (translation (scale (distance origin centre - radius) v)) origin
          q = transform (translation (scale (distance origin centre + radius) v)) origin
          p' = embedPoint poincareDisc p
          q' = embedPoint poincareDisc q
          centre' = midpoint p' q'
  , unembedConstruct = error "unembedConstruct poincareDisc"
  }

poincareHalfPlane :: RealFloat r => Embed (H r) (E r)
poincareHalfPlane = Embed
  { embedPoint = \(HPoint z w) -> EPoint (-i * (z + w) / (z - w))
  , unembedPoint = \(EPoint z@(_ :+ y)) -> if y > 0 then Just (HPoint (z - i) (z + i)) else Nothing
  , embedConstruct = \c -> case c of
      HLine p q -> case intersect (perpendicularBisector p' q') (line origin (EPoint 1)) of
        [c'@(EPoint (cx :+ _))] | abs cx < 1e6 -> circle c' p'
        _ -> line p' q'
        where
          p' = embedPoint poincareHalfPlane p
          q' = embedPoint poincareHalfPlane q
      HCircle p r ->
        let EPoint (a :+ b) = embedPoint poincareHalfPlane p
            y = b * cr
            z = b * sqrt (cr * cr - 1)
            cr = cosh r
        in  circle (EPoint (a :+ y)) (EPoint (a :+ (y + z)))
  , unembedConstruct = error "unembedConstruct poincareHalfPlane"
  }
  where
    i = 0 :+ 1

instance RealFloat r => Compass (H r) where
    type Scalar (H r) = r
    data Point (H r) = HPoint !(Complex r) !(Complex r)
    data Vector (H r) = HVector !(Complex r)
    data Construct (H r) = HLine !(Point (H r)) !(Point (H r)) | HCircle !(Point (H r)) !r
    data Transform (H r) = HTransform !(Complex r) !(Complex r) !(Complex r) !(Complex r)
    origin = HPoint 0 1
    one = HVector 1
    vector p@(HPoint z w) = HVector (mkPolar (distance p origin) (phase (z / w)))
    line = HLine
    circle p q = HCircle p (distance p q)

    rotate a (HVector v) = HVector (cis a * v)
    scale s (HVector (x :+ y)) = HVector (s * x :+ s * y)

    intersect l m = mapMaybe (unembedPoint poincareDisc) $ intersect (embedConstruct poincareDisc l) (embedConstruct poincareDisc m)
    
    distance p1 p2 = acosh (1 + 2 * magnitudeSquared (u - v) / ((1 - magnitudeSquared u) * (1 - magnitudeSquared v)))
      where
        u = fromPoint p1
        v = fromPoint p2
    
    angle p1 p2 p3 = angle (t p1) origin (t p3)
      where
        t = embedPoint poincareDisc . transform (translation . scale (-1) . vector $ p2)
    
    compose (HTransform a b c d) (HTransform u v x y) = HTransform (a * u + b * x) (a * v + b * y) (c * u + d * x) (c * v + d * y)
    
    transform (HTransform a b c d) (HPoint z w) = HPoint (a * z + b * w) (c * z + d * w)
    
    translation (HVector v)
      | l > 0 = rotation s `compose` m `compose` rotation (negate s)
      | otherwise = HTransform  1 0 0 1
      where
        (l, s) = polar v
        e = exp l
        f = (e + 1) :+ 0
        g = (e - 1) :+ 0
        m = HTransform f g g f
    
    rotation a = HTransform (cis a) 0 0 1

fromPoint (HPoint z w) = z / w

magnitudeSquared (x :+ y) = x * x + y * y

normalize (HVector v)
  | v == 0 = HVector 1
  | otherwise = HVector (signum v)

deriving instance Read r => Read (Point (H r))
deriving instance Read r => Read (Vector (H r))
deriving instance Read r => Read (Construct (H r))
deriving instance Read r => Read (Transform (H r))
deriving instance Show r => Show (Point (H r))
deriving instance Show r => Show (Vector (H r))
deriving instance Show r => Show (Construct (H r))
deriving instance Show r => Show (Transform (H r))
deriving instance Eq r => Eq (Point (H r))
deriving instance Eq r => Eq (Vector (H r))
deriving instance Eq r => Eq (Construct (H r))
deriving instance Eq r => Eq (Transform (H r))
