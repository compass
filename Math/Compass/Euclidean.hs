{-# LANGUAGE TypeFamilies, StandaloneDeriving, FlexibleInstances #-}
module Math.Compass.Euclidean where

import Math.Compass.Class

import Data.Complex (Complex((:+)), magnitude, phase, cis)

data E r = E

instance RealFloat r => Compass (E r) where
    type Scalar (E r) = r
    data Point (E r) = EPoint !(Complex r)
    data Vector (E r) = EVector !(Complex r)
    data Construct (E r) = ELine !(Point (E r)) !(Point (E r)) | ECircle !(Point (E r)) !r
    data Transform (E r) = ETransform !r !r !r !r !r !r
    origin = EPoint 0
    one = EVector 1
    vector (EPoint p) = EVector p
    line = ELine
    circle p q = ECircle p (distance p q)

    intersect (ELine (EPoint (x1 :+ y1)) (EPoint (x2 :+ y2))) (ELine (EPoint (x3 :+ y3)) (EPoint (x4 :+ y4)))
--      | m == 0 || n == 0 = [] -- coincident
      | d == 0 = [] -- parallel
      | otherwise = [EPoint (x :+ y)]
      where
        d = (y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1)
        m = (x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3)
        n = (x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3)
        x = x1 + (x2 - x1) * m / d
        y = y1 + (y2 - y1) * m / d
    
    intersect (ELine (EPoint (x1 :+ y1)) (EPoint (x2 :+ y2))) (ECircle (EPoint (x3 :+ y3)) r3)
      | d == 0 = [EPoint ((x1 + u  * (x2 - x1)) :+ (y1 + u  * (y2 - y1)))]
      | d >  0 = [EPoint ((x1 + u1 * (x2 - x1)) :+ (y1 + u1 * (y2 - y1))), EPoint ((x1 + u2 * (x2 - x1)) :+ (y1 + u2 * (y2 - y1)))]
      | otherwise = []
      where
        a = (x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1)
        b = 2 * ((x2 - x1)*(x1 - x3) + (y2 - y1)*(y1 - y3))
        c = x3*x3 + y3*y3 + x1*x1 + y1*y1 - 2*(x3*x1 + y3*y1) - r3*r3
        d = b*b - 4 * a * c
        u  =  -b / (2 * a)
        u1 = (-b + sqrt d) / (2 * a)
        u2 = (-b - sqrt d) / (2 * a)
    
    intersect (ECircle (EPoint (x0 :+ y0)) r0) (ECircle (EPoint (x1 :+ y1)) r1)
      | d == 0 && r0 == r1 = [] -- coincident
      | d == r0 + r1 = [EPoint (x2 :+ y2)]
      | d >  r0 + r1 = [] -- disjoint
      | d <  abs (r0 - r1) = [] -- concentric
      | otherwise = [EPoint ((x2 + x3) :+ (y2 + y3)), EPoint ((x2 - x3) :+ (y2 - y3))]
      where
        dx = x1 - x0
        dy = y1 - y0
        d  = sqrt(dx * dx + dy * dy)
        a  = (r0 * r0 - r1 * r1 + d * d) / (2 * d);
        h  = sqrt(r0 * r0 - a * a)
        x2 = x0 + a * (x1 - x0) / d
        y2 = y0 + a * (y1 - y0) / d
        x3 =  h * (y1 - y0) / d
        y3 = -h * (x1 - x0) / d
    
    intersect c l = intersect l c
    
    distance (EPoint p) (EPoint q) = magnitude (p - q)

    angle (EPoint p1) (EPoint p2) (EPoint p3) = phase ((p1 - p2) / (p3 - p2))
    
    scale s (EVector (x :+ y))= EVector (s * x :+ s * y)
    
    rotate a (EVector v) = EVector (cis a * v)
    
    compose (ETransform a b c d e f) (ETransform u v w x y z) = ETransform (a * u + b * x) (a * v + b * y) (a * w + b * z + c) (d * u + e * x) (d * v + e * y) (d * w + e * z + f)
    
    transform (ETransform a b c d e f) (EPoint (x :+ y)) = EPoint ((a * x + b * y + c) :+ (d * x + e * y + f))
    
    translation (EVector (x :+ y)) = ETransform 1 0 x 0 1 y
    
    rotation a = ETransform c (-s) 0 s c 0
      where
        c :+ s = cis a

    midpoint (EPoint p) (EPoint q) = EPoint ((p + q) / 2)

deriving instance Read r => Read (Point (E r))
deriving instance Read r => Read (Vector (E r))
deriving instance Read r => Read (Construct (E r))
deriving instance Read r => Read (Transform (E r))
deriving instance Show r => Show (Point (E r))
deriving instance Show r => Show (Vector (E r))
deriving instance Show r => Show (Construct (E r))
deriving instance Show r => Show (Transform (E r))
deriving instance Eq r => Eq (Point (E r))
deriving instance Eq r => Eq (Vector (E r))
deriving instance Eq r => Eq (Construct (E r))
deriving instance Eq r => Eq (Transform (E r))

euclidean :: Embed (E r) (E r)
euclidean = Embed
  { embedPoint = id
  , unembedPoint = Just
  , embedConstruct = id
  , unembedConstruct = Just
  }
