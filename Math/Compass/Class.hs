{-# LANGUAGE TypeFamilies #-}
module Math.Compass.Class where

class Compass s where
  type Scalar s :: *
  data Point s :: *
  data Vector s :: *
  data Construct s :: *
  data Transform s :: *
  origin :: Point s
  one :: Vector s
  vector :: Point s -> Vector s
  line :: Point s -> Point s -> Construct s
  circle :: Point s -> Point s -> Construct s
  intersect :: Construct s -> Construct s -> [Point s]
  distance :: Point s -> Point s -> Scalar s
  angle :: Point s -> Point s -> Point s -> Scalar s
  scale :: Scalar s -> Vector s -> Vector s
  rotate :: Scalar s -> Vector s -> Vector s
  compose :: Transform s -> Transform s -> Transform s
  transform :: Transform s -> Point s -> Point s
  translation :: Vector s -> Transform s
  rotation :: Scalar s -> Transform s

  perpendicularBisector :: Point s -> Point s -> Construct s
  perpendicularBisector p q = l
    where
      c = circle p q
      c' = circle q p
      [p', q'] = intersect c c'
      l = line p' q'

  midpoint :: Point s -> Point s -> Point s
  midpoint p q = m
    where
      l = line p q
      l' = perpendicularBisector p q
      [m] = intersect l l'

data Embed s t = Embed
  { embedPoint :: Point s -> Point t
  , unembedPoint :: Point t -> Maybe (Point s)
  , embedConstruct :: Construct s -> Construct t
  , unembedConstruct :: Construct t -> Maybe (Construct s)
  }
