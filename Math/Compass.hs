{-# LANGUAGE MultiParamTypeClasses, FlexibleInstances, FunctionalDependencies #-}
module Math.Compass
  ( module Math.Compass.Class
  , module Math.Compass.Euclidean
  , module Math.Compass.Hyperbolic
  ) where

import Math.Compass.Class
import Math.Compass.Euclidean
import Math.Compass.Hyperbolic
